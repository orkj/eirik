<?php
// $Id$
/**
 * @file
 * Form builder for Delving Remote Search API settings.
 *
 * @ingroup forms
 * @see system_settings_form()
 */

header('Content-Type: text/html; charset=utf-8');

function eirik_admin_settings($form, &$form_state)
{

  $form['eirik_content_field'] = array(
    '#type' => 'textfield',
    '#title' => t('Field to use for search word'),
    '#default_value' => variable_get('eirik_content_field', EIRIK_CONTENT_FIELD),
    '#required' => TRUE,
    '#description' => t('Full machine-readable name (like field_county)')
  );

  $form['eirik_is_taxonomy'] = array(
    '#type' => 'radios',
    '#title' => t('Is the data field a taxonomy/term reference?'),
    '#default_value' => variable_get('eirik_is_taxonomy', EIRIK_IS_TAXONOMY),
    '#options' => array('true' => t('Yes sir! It is a taxonomy.'), 'false' => t('Nope.')),
  );

  $form['eirik_use_delving_facet'] = array(
    '#type' => 'select',
    '#title' => t('What delving facet would you like to use the field for?'),
    '#default_value' => variable_get('eirik_use_delving_facet', EIRIK_USE_DELVING_FACET),
    '#options' => array('abm_municipality_facet' => t('Municipality'), 'COUNTY' => t('County'), 'DATAPROVIDER' => t('Data provider'), 'NAMEDPLACE' => t('Named place')),
  );


  return system_settings_form($form);
}
